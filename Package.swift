// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftEditorPDFHelper",
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(name: "SwiftEditorPDFHelper",
                 targets: ["SwiftEditorPDFHelper"])
    ],
    dependencies: [
    ],
    targets: [
        .binaryTarget(name: "EditorPDFHelper", 
                      path: "Sources/EditorPDFHelper.xcframework"),
        .target(name: "SwiftEditorPDFHelper")
    ]
)
